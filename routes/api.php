<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('err', 'ErrorsController@PostError');

Route::post('point', 'PointsController@PostPoint');

Route::post('track', 'TracksController@PostTrack');

Route::patch('upTrack', 'TracksController@updateTrack');

Route::get('/tracks/id/{id}', 'TracksController@STrackById');

Route::get('/tracks/point/{id}', 'TracksController@SGetTrackByPointId');

Route::get('/tracks/{strName}', 'TracksController@STrackByName');

Route::get('/tourists/{name}', 'TouristsController@STouristByName');

Route::get('/tourists/id/{id}', 'TouristsController@STouristById');

Route::get('/mountaingroups', 'MountainGroupController@SGetAllMountainGroups');

Route::get('/points/id/{id}', 'PointsController@SGetPointByMountainGroupId');

Route::get('/points/{pointsValue}/{operator}/{mountainGroupId}', 'PointsController@SGetPointsByTrackPointsValueAndMountainGroupId');

Route::get('/err/{description}/{priority}/{photo}/{cords}/{touristId}', 'ErrorsController@PutError');#cords are sent with json object