<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 05.01.2019
 * Time: 16:42
 */

namespace App\Http\Middleware;


use App\Http\Middleware\DAO\ErrorDAO;

class Utility
{
    public static function replace_WithSpace ($string){
        return str_replace('_', ' ', $string);
    }
    public static function JSON_with_polish_chars($string){
        $ustr = array('\u0104','\u0106','\u0118','\u0141','\u0143','\u00d3','\u015a','\u0179','\u017b','\u0105','\u0107','\u0119','\u0142','\u0144','\u00f3','\u015b','\u017a','\u017c');
        $plstr = array('Ą','Ć','Ę','Ł','Ń','Ó','Ś','Ź','Ż','ą','ć','ę','ł','ń','ó','ś','ź','ż');

        $json = str_replace($ustr,$plstr,json_encode($string, JSON_PRETTY_PRINT));
        return $json;
    }
    public static function splitOn_($string){
        $arr = explode('_', $string);
        return $arr;
    }
    public static function getOperatorSignFromString($operator){
        $result = '';
        if ($operator == 'lower'){
            $result = '<';
        }
        elseif ($operator == 'greater'){
            $result = '>';
        }
        else{
            $result = '=';
        }
        return $result;
    }
    public static function savePhotoOnDisc($photo){
        $id = ErrorDAO::getLastErrorId();
        $p = "C:\\xampp\\htdocs\\laravel\\public\\uploads\\$id.png";

        file_put_contents($p,file($photo));

        return $p;
    }
}