<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 05.01.2019
 * Time: 16:24
 */

namespace App\Http\Middleware;


use App\Models\Trip;

class TripInitializer
{
    public static function createTripObject($trips, $tracksFromTripObjects, $tftcons){
        $tripObjects = [];

        foreach($trips as $trip) {
            $tripObject = new Trip($trip->Id, $trip->DataPoczatku, $trip->DataKonca, $trip->Nazwa, $trip->Punkty);

            $tracks = [];

            for ($i = 0; $i < count($tracksFromTripObjects); $i++) {
                if ($tftcons[$i] == $trip->Id) {
                    array_push($tracks, $tracksFromTripObjects[$i]);
                }
            }

            $tripObject->setTracks($tracks);
            array_push($tripObjects, $tripObject);
        }
        return $tripObjects;

    }
    public static function createTripConnectionIds($trips){
        $TripConIds = [];

        foreach ($trips as $trip) {
            array_push($TripConIds, $trip -> TurystaId);
        }
        return $TripConIds;
    }
}