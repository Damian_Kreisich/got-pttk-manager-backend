<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 05.01.2019
 * Time: 03:07
 */

namespace App\Http\Middleware\DAO;


use Illuminate\Support\Facades\DB;

class PointDAO
{
    public static function getPointsByTrackIds($trackIds){
        $points = DB::table('punkt_wycieczki')
            -> select(DB::raw('punkt_wycieczki.Id, punkt_wycieczki.Nazwa, punkt_wycieczki.Wysokosc, grp.GrupaGórska, pan.Panstwo, tpp.Trasa_PTTKId'))
            -> join('trasa_pttk_punkt_wycieczki as tpp', 'tpp.Punkt_wycieczkiId', '=', 'punkt_wycieczki.Id' )
            -> join('grupa_górska as grp', 'grp.Id', '=', 'punkt_wycieczki.Grupa_górskaId')
            -> join('panstwo as pan', 'pan.Id','=', 'punkt_wycieczki.PanstwoId')
            -> whereIn('tpp.Trasa_PTTKId', $trackIds)
            -> orderBy('tpp.Trasa_PTTKId')
            -> orderBy('punkt_wycieczki.Id')
            ->get();

        return $points;
    }
    public static function getPointsByMountainGroupId($id){
        if (!is_array($id)){
            $id = [$id];
        }
        $points = DB::table('punkt_wycieczki')
            -> select(DB::raw('punkt_wycieczki.Id, punkt_wycieczki.Nazwa, punkt_wycieczki.Wysokosc, grp.GrupaGórska, pan.Panstwo'))
            -> join('grupa_górska as grp', 'grp.Id', '=', 'punkt_wycieczki.Grupa_górskaId')
            -> join('panstwo as pan', 'pan.Id','=', 'punkt_wycieczki.PanstwoId')
            -> whereIn('grp.Id', $id)
            -> get();

        return $points;
    }
    public static function getPointsByPointsValueAndMountainGroup($pointsValue, $operator, $mountainGroupId){
        if (!is_array($mountainGroupId)){
            $mountainGroupId = [$mountainGroupId];
        }

        $points = DB::table('punkt_wycieczki')
            -> select(['punkt_wycieczki.Id', 'punkt_wycieczki.Nazwa', 'punkt_wycieczki.Wysokosc', 'grp.GrupaGórska', 'pan.Panstwo','tp.PunktyWGore', 'tp.PunktyWDol'])
            -> join('trasa_pttk_punkt_wycieczki as tpp', 'tpp.Punkt_wycieczkiId', '=', 'punkt_wycieczki.Id' )
            -> join('grupa_górska as grp', 'grp.Id', '=', 'punkt_wycieczki.Grupa_górskaId')
            -> join('panstwo as pan', 'pan.Id','=', 'punkt_wycieczki.PanstwoId')
            -> join('trasa_pttk as tp', 'tp.Id', '=', 'tpp.Trasa_PTTKId')
            -> whereIn('grp.Id', $mountainGroupId)
            -> distinct()
            -> get();

        $finalpoints = [];
        foreach($points as $point) {
            $actPointsVal = max([$point->PunktyWGore, $point->PunktyWDol]);

            $inFinal = false;

            $i = 0;
            while(!$inFinal and $i < count($finalpoints)){
                $inFinal = $finalpoints[$i] -> Id == $point -> Id;
                $i++;
            }

            if (!$inFinal) {

                switch ($operator) {
                    case '>':
                        if ($actPointsVal > $pointsValue) {
                            array_push($finalpoints, $point);
                        }
                        break;
                    case '<':
                        if ($actPointsVal < $pointsValue) {
                            array_push($finalpoints, $point);
                        }
                        break;
                    case '=':
                        if ($point->PunktyWGore == $pointsValue or $point->PunktyWDol == $pointsValue) {
                            array_push($finalpoints, $point);
                        }
                        break;
                }
            }
        }
        return $finalpoints;
    }
    public static function insertPointObject($pointObject){

        try {
            $exception = DB::transaction(function() use ($pointObject) {
                $pointId = self::insertQueryCreatePoint($pointObject);
                $conIds = CordsDAO::insertCordsWhenNotInDB([$pointObject->getLatitude(), $pointObject->getLongitude()]);
                self::insertQueryCreateConnections($pointId, $conIds);
            });
                return is_null($exception) ? 'true' : $exception;

            } catch (\Exception $e) {
                return 'false';
            }
    }
    private static function insertQueryCreatePoint($pointObject)
    {
            $pointId = DB::table('punkt_wycieczki') -> insertGetId(
                [
                    'Id' => 0,
                    'Nazwa' => $pointObject -> getName(),
                    'Wysokosc' => $pointObject -> getAltitude(),
                    'Grupa_górskaId' => $pointObject -> getMountainGroup(),
                    'PanstwoId' => 1
                ]
            );
        return $pointId;
    }
    private static function insertQueryCreateConnections($pointId, $conIds){
        foreach ($conIds as $conId){
            DB::table('wspolrzedne_punkt_wycieczki') -> insert(
                [
                    'WspolrzedneId' => $conId,
                    'Punkt_wycieczkiId' => $pointId
                ]
            );
        }
    }
}