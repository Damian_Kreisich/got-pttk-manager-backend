<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 06.01.2019
 * Time: 00:09
 */

namespace App\Http\Middleware\DAO;


use Illuminate\Support\Facades\DB;

class MountainGroupDAO
{
    public static function getAllMountainGroups(){
        $mountainGroups = DB::table('grupa_górska')
            ->get();
        return $mountainGroups;
    }
    public static function getMountainGroupsByTouristId($id){
        if (!is_array($id)){
            $id = [$id];
        }

        $mountainGroups = DB::table('grupa_górska')
            -> select(DB::raw('grupa_górska.Id, grupa_górska.GrupaGórska'))
            -> join('turysta_grupa_górska as s', 's.Grupa_górskaId', '=', 'grupa_górska.Id')
            -> whereIn('s.TurystaId', $id)
            -> get();

        return $mountainGroups;
    }
}