<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 05.01.2019
 * Time: 15:44
 */

namespace App\Http\Middleware\DAO;


use Illuminate\Support\Facades\DB;

class TouristDAO
{
    public static function getTouristByName($name){
        $tourists = DB::table('turysta')
            -> select(DB::raw('turysta.Id, turysta.DataUrodzenia, turysta.Punkty, turysta.CzyNiepelnosprawny, turysta.Imie,
             turysta.Nazwisko, turysta.PESEL, turysta.NrTel, turysta.czyPrzodownik, turysta.NrLegitymacji, m.Miasto, w.Wojewodztwo'))
            ->join('miasto as m','m.Id','=', 'turysta.MiastoId')
            ->join('wojewodztwo as w', 'w.Id', '=', 'turysta.WojewodztwoId')
            ->whereRaw('CONCAT(turysta.Imie, \'_\', turysta.Nazwisko) LIKE '.'\'%'.$name.'%\'')
            ->get();
        return $tourists;
    }
    public static function getTouristById($id){
        $tourists = DB::table('turysta')
            -> select(DB::raw('turysta.Id, turysta.DataUrodzenia, turysta.Punkty, turysta.CzyNiepelnosprawny, turysta.Imie,
             turysta.Nazwisko, turysta.PESEL, turysta.NrTel, turysta.czyPrzodownik, turysta.NrLegitymacji, m.Miasto, w.Wojewodztwo'))
            ->join('miasto as m','m.Id','=', 'turysta.MiastoId')
            ->join('wojewodztwo as w', 'w.Id', '=', 'turysta.WojewodztwoId')
            ->where('turysta.Id' ,'=', $id)
            ->get();
        return $tourists;
    }
}