<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 05.01.2019
 * Time: 16:25
 */

namespace App\Http\Middleware\DAO;


use Illuminate\Support\Facades\DB;

class TripDAO
{
    public static function getTripsByTouristsId($ids){
        $trips = DB::table('wycieczka')
            -> select(DB::raw('wycieczka.Id, wycieczka.DataPoczatku, wycieczka.DataKonca, wycieczka.Nazwa, wycieczka.Punkty, wycieczka.TurystaId'))
            -> whereIn('wycieczka.TurystaId', $ids)
            -> get();

        return $trips;
    }
}