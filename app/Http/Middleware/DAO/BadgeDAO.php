<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 05.01.2019
 * Time: 23:18
 */

namespace App\Http\Middleware\DAO;


use Illuminate\Support\Facades\DB;

class BadgeDAO
{
    public static function getLatestBadgeByTouristId($id){
        $badge = DB::table('got')
            -> select(DB::raw('got.DataZdobycia, got.DataRozpoczecia, got.PotrzebnePunkty, got.Id, s.Stopien, r.Rodzaj_odznaki, got.normy'))
            -> join('stopien as s', 's.Id', '=', 'got.StopienId')
            -> join('rodzaj_odznaki as r', 'r.Id', '=', 'got.Rodzaj_odznakiId')
            -> where('TurystaId','=',$id, 'and')
            -> whereNull('DataZdobycia')
            -> get();
        return $badge;
    }
}