<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 05.01.2019
 * Time: 16:25
 */

namespace App\Http\Middleware\DAO;


use Illuminate\Support\Facades\DB;

class TrackFromTripDAO
{
    public static function getTrackFromTripByTripId($ids){
        $TrackFromTrips = DB::table('trasa_przebyta')
            ->select(DB::raw('trasa_przebyta.Id, trasa_przebyta.DataOd, trasa_przebyta.DataDo, trasa_przebyta.CzyWlasna,
             trasa_przebyta.CzyWDol, trasa_przebyta.Trasa_PTTKId, trasa_przebyta.WycieczkaId'))
            ->whereIn('trasa_przebyta.WycieczkaId', $ids)
            ->get();
        return $TrackFromTrips;
    }
}