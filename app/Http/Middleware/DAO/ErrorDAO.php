<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 06.01.2019
 * Time: 12:29
 */

namespace App\Http\Middleware\DAO;


use Illuminate\Support\Facades\DB;
use app\Models\TrackError;

class ErrorDAO
{
    public static function insertErrorObject($errorObject)
    {

        try {
            $exception = DB::transaction(function() use ($errorObject){
                $errorId = self::insertQueryCreateError($errorObject);
                if ($errorObject -> getLatitude() != null and $errorObject -> getLongitude() != null){
                    $conIds = CordsDAO::insertCordsWhenNotInDB([$errorObject -> getLatitude(), $errorObject -> getLongitude()]);
                    self::insertQueryCreateConnections($errorId, $conIds);
                }
            });
            return is_null($exception) ? 'true' : $exception;

        } catch (Exception $e) {
            return 'false';
        }
    }
    private static function insertQueryCreateError($errorObject)
    {
        $errorId = 0;
        #workaround the blob object problem
        if ($errorObject -> getEvidence() == 'NULL'){
            $errorId = DB::table('błąd') -> insertGetId(
                [
                    'Id' => 0,
                    'DataZgloszenia' => $errorObject -> getFilingDate(),
                    'Opis' => $errorObject -> getDescription(),
                    'Dowód' => null,
                    'TurystaId' => $errorObject -> getDeclarant(),
                    'PriorytetId' => $errorObject -> getPriority()
                ]
            );
        }
        else{
            $errorId = DB::table('błąd') -> insertGetId(
                [
                    'Id' => 0,
                    'DataZgloszenia' => $errorObject -> getFilingDate(),
                    'Opis' => $errorObject -> getDescription(),
                    'Dowód' => $errorObject -> getEvidence(),
                    'TurystaId' => $errorObject -> getDeclarant(),
                    'PriorytetId' => $errorObject -> getPriority()
                ]
            );
        }
        return $errorId;
    }
    private static function insertQueryCreateConnections($errorId, $conIds){
        foreach ($conIds as $conId){
        DB::table('wspolrzedne_błąd') -> insert(
                [
                    'WspolrzedneId' => $conId,
                    'BłądId' => $errorId
                ]
            );
        }
    }
    public static function getLastErrorId(){
        $errorId = DB::table('błąd') -> max('Id');
        return $errorId;
    }
}