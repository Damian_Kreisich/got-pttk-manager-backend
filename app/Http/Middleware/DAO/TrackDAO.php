<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 05.01.2019
 * Time: 03:00
 */

namespace App\Http\Middleware\DAO;



use App\Models\TrackPTTK;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Helper\Table;


class TrackDAO
{
    public static function getTrackByPointOrTrackName($strName){
        $tracks = DB::table('trasa_pttk_punkt_wycieczki')
            ->select(DB::raw(' DISTINCT trasa_pttk.Id,  trasa_pttk.Nazwa as Nazwa_trasy, trasa_pttk.PunktyWGore, 
            trasa_pttk.PunktyWDol, trasa_pttk.ObowiązujeDo, trasa_pttk.ObowiązujeOd  '))
            ->join('trasa_pttk', 'trasa_pttk_punkt_wycieczki.Trasa_PTTKId',
                '=', 'trasa_pttk.Id')
            ->join('punkt_wycieczki', 'trasa_pttk_punkt_wycieczki.Punkt_wycieczkiId',
                '=', 'punkt_wycieczki.Id')
            ->where('trasa_pttk.Nazwa', 'LIKE', '%' . $strName . '%')
            ->whereNull("trasa_pttk.ObowiązujeDo")
            ->orWhere('punkt_wycieczki.Nazwa', 'LIKE', '%' . $strName . '%')
            ->whereNull("trasa_pttk.ObowiązujeDo")
            ->orderBy('trasa_pttk.Id')
            ->get();
        return $tracks;
    }
    public static function getTrackById($id){
        if (!is_array($id))
            $id = [$id];
        $tracks = DB::table('trasa_pttk')
            ->select(DB::raw(' DISTINCT trasa_pttk.Id,  trasa_pttk.Nazwa as Nazwa_trasy, trasa_pttk.PunktyWGore, 
            trasa_pttk.PunktyWDol, trasa_pttk.ObowiązujeDo, trasa_pttk.ObowiązujeOd  '))
            ->whereIn('Id', $id)
            ->whereNull("trasa_pttk.ObowiązujeDo")
            ->get();
        return $tracks;
    }
    public static function getTrackByPointId($id){
        if (!is_array($id))
            $id = [$id];

        $tracks = DB::table('trasa_pttk')
            ->select(DB::raw(' DISTINCT trasa_pttk.Id,  trasa_pttk.Nazwa as Nazwa_trasy, trasa_pttk.PunktyWGore, 
            trasa_pttk.PunktyWDol, trasa_pttk.ObowiązujeDo, trasa_pttk.ObowiązujeOd  '))
            ->join('trasa_pttk_punkt_wycieczki as p', 'p.Trasa_PTTKId', '=', 'trasa_pttk.Id')
            ->whereIn('p.Punkt_wycieczkiId', $id)
            ->whereNull("trasa_pttk.ObowiązujeDo")
            ->get();

        return $tracks;
    }
    public static function insertTrackObject(TrackPTTK $trackObject){
        try {
            $exception = DB::transaction(function() use ($trackObject) {
                $trackId = self::insertTrackQuery($trackObject);
                self::insertQueryCreateConnections($trackId,
                    [$trackObject->getPointA(), $trackObject->getPointB()]);
            });
            return is_null($exception) ? 'true' : $exception;

        } catch (Exception $e) {
            return 'false';
        }
    }
    private static function insertTrackQuery(TrackPTTK $trackObject){
        $id = DB::table('trasa_pttk') -> insertGetId(
            [
                'Nazwa' => $trackObject -> getName(),
                'Id' => $trackObject -> getId(),
                'PunktyWGore' => $trackObject -> getPointsUp(),
                'PunktyWDol' => $trackObject -> getPointsDown(),
                'ObowiązujeOd' => $trackObject -> getValidFrom(),
                'ObowiązujeDo' => $trackObject -> getValidTo()
            ]);

        return $id;
    }
    private static function insertQueryCreateConnections($trackId, $conIds){
        foreach ($conIds as $conId){
        DB::table('trasa_pttk_punkt_wycieczki') -> insert(
            [
                'Id' => 0,
                'Punkt_wycieczkiId' => $conId,
                'Trasa_PTTKId' => $trackId
            ]
        );
            }
    }
    public static function updateTrackExpDateById($id){
        $exception = DB::table("trasa_pttk")
            -> where("Id", $id)
            -> update(
                [
                    "ObowiązujeDo" => date('Y-m-j')
                ]
            );
        return $exception;
    }
}