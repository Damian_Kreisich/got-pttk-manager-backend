<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 05.01.2019
 * Time: 03:12
 */

namespace App\Http\Middleware\DAO;


use Illuminate\Support\Facades\DB;

class CordsDAO
{
    public static function getCordsByPointIds($pointsIds){

        $cords = DB::table('wspolrzedne_punkt_wycieczki')
            -> select(DB::raw('w.Id, w.stopnie, w.minuty, w.sekundy ,wspolrzedne_punkt_wycieczki.Punkt_wycieczkiId'))
            -> join('wspolrzedne as w', 'w.Id', '=', 'wspolrzedne_punkt_wycieczki.WspolrzedneId')
            -> whereIn('wspolrzedne_punkt_wycieczki.Punkt_wycieczkiId', $pointsIds)
            -> orderBy('wspolrzedne_punkt_wycieczki.Punkt_wycieczkiId')
            -> get();

        return $cords;
    }
    public static function insertCordsWhenNotInDB($cordObjects)
    {

        $cordIds = [];

        foreach ($cordObjects as $cord) {
            $id = DB::table('wspolrzedne')->insertGetId(
                [
                    'Id' => 0,
                    'Stopnie' => $cord -> getDegrees(),
                    'Minuty' => $cord -> getMinutes(),
                    'Sekundy' => $cord -> getSeconds()
                ]);
            array_push($cordIds, $id);
        }
        return $cordIds;
    }
}