<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 05.01.2019
 * Time: 15:44
 */

namespace App\Http\Middleware\Initializers;


use App\Http\Middleware\DAO\BadgeDAO;
use App\Http\Middleware\DAO\MountainGroupDAO;
use App\Http\Middleware\TripInitializer;
use App\Models\Tourist;

class TouristInitializer
{
    public static function createTouristObject($tourists, $trips, $tracksFromTrip, $tracks, $points, $cords){
        $touristObjects = [];


        $tracksObjects = TracksInitializer::createTrackObject($tracks, $points, $cords);
        $tracksFromTripObjects = TrackFromTripInitializer::createTFTObject($tracksFromTrip, $tracksObjects);
        $tftCons = TrackFromTripInitializer::createTFTConnectionIds($tracksFromTrip);
        $tripObjects = TripInitializer::createTripObject($trips, $tracksFromTripObjects, $tftCons);
        $tripCons = TripInitializer::createTripConnectionIds($trips);
        foreach($tourists as $tourist){
            $touristObject = new Tourist($tourist -> Id, $tourist -> DataUrodzenia, $tourist -> Punkty, $tourist->NrLegitymacji,
                $tourist -> CzyNiepelnosprawny, $tourist -> czyPrzodownik, $tourist -> Imie, $tourist -> Nazwisko, $tourist -> PESEL,
                $tourist -> NrTel, $tourist -> Miasto, $tourist -> Wojewodztwo);
            $tripArr = [];
            for($i = 0;$i < count($tripObjects); $i++){
                if($tripCons[$i] == $tourist -> Id){
                    array_push($tripArr, $tripObjects[$i]);
                }
            }

            $badge = BadgeInitializer::createBadgeObject(BadgeDAO::getLatestBadgeByTouristId($tourist -> Id));
            $mountains = MountainGroupInitializer::createMountainGroupObjects(MountainGroupDAO::getMountainGroupsByTouristId($tourist -> Id));
            $touristObject -> setTrips($tripArr);
            $touristObject -> setLatestBadge($badge[0]);
            $touristObject -> setPriviliges($mountains);
            array_push($touristObjects, $touristObject);
        }

        return $touristObjects;
    }
}