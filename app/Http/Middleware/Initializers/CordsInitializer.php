<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 05.01.2019
 * Time: 14:56
 */

namespace App\Http\Middleware\Initializers;
use App\Http\Controllers\PointsController;
use App\Models\PointCoordinates;

class CordsInitializer
{
    public static function createCordsObject($cords){
        $cordObjects = array();

        foreach ($cords as $cord) {
            array_push($cordObjects, new PointCoordinates($cord->Id, $cord->stopnie, $cord->minuty, $cord->sekundy));
        }

        return $cordObjects;
    }
    public static function createCordsConnectionIds($cords){
        $cordConIds = [];

        foreach ($cords as $cord) {
            array_push($cordConIds, $cord -> Punkt_wycieczkiId);
        }

        return $cordConIds;
    }
    public static function createCordsObjectsFromJson($cords){
        $cordObjects = [];

        foreach($cords as $cord){
            $cordObject = new PointCoordinates($cord['id'], $cord['degrees'], $cord['minutes'], $cord['seconds']);

            array_push($cordObjects, $cordObject);
        }

        return $cordObjects;
    }
    public static function createCordsObjectsFromStringData($latitudedegrees, $latitudeminutes, $latitudeseconds,
                                                            $longitudedegrees, $longitudeminutes, $logintudeseconds){
        $cords = [];
        array_push($cords, new PointCoordinates(0, $latitudedegrees, $latitudeminutes, $latitudeseconds));
        array_push($cords, new PointCoordinates(0,$longitudedegrees, $longitudeminutes, $logintudeseconds));
        return $cords;
    }
}