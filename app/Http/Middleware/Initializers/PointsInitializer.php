<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 05.01.2019
 * Time: 14:56
 */

namespace App\Http\Middleware\Initializers;


use App\Models\TrackPoint;

class PointsInitializer
{
    public static function createPointObject($points, $cordObjects, $cordObjectConnectionIds){
        $pointObjects = array();

        foreach ($points as $point) {
            $trackpoint =  new TrackPoint($point -> Id, $point -> Nazwa, $point -> Wysokosc,
                $point -> GrupaGórska, $point -> Panstwo);

            $cordIndexes = [];

            for($j=0; $j < count($cordObjectConnectionIds);$j++){
                if ($cordObjectConnectionIds[$j] == $point -> Id){
                    array_push($cordIndexes, $j);
                }
            }
            $trackpoint -> setLatitude($cordObjects[$cordIndexes[0]]);
            $trackpoint -> setLongitude($cordObjects[$cordIndexes[1]]);

            array_push($pointObjects, $trackpoint);
        }

        return $pointObjects;
    }
    public static function createPointsConnectionIds($points){
        $pointConIds = [];

        foreach ($points as $point) {
            array_push($pointConIds, $point -> Trasa_PTTKId);
        }

        return $pointConIds;
    }
    public static function pcreatePointObject($id, $name, $altitude, $cords, $mountainGroup, $country){
        $cords = CordsInitializer::createCordsObjectsFromStringData($cords[0][0], $cords[0][1], $cords [0][2],
            $cords[1][0], $cords[1][1], $cords[1][2]);

        $latitude = $cords[0];
        $longitude = $cords[1];

        $pointObject = new TrackPoint($id, $name, $altitude, $mountainGroup, $country);

        $pointObject->setLatitude($latitude);
        $pointObject->setLongitude($longitude);
        return $pointObject;
    }
}