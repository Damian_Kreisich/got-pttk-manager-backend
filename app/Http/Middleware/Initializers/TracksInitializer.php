<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 05.01.2019
 * Time: 14:56
 */

namespace App\Http\Middleware\Initializers;

use App\Models\TrackPTTK;

class TracksInitializer
{
    public static function createTrackObject($tracks, $points, $cords)
    {
        $trackObjects = array();

        $cordObjects = CordsInitializer::createCordsObject($cords);
        $cordObjectConnectionIds = CordsInitializer::createCordsConnectionIds($cords);

        $pointObjects = PointsInitializer::createPointObject($points, $cordObjects, $cordObjectConnectionIds);
        $pointObjectConnectionIds = PointsInitializer::createPointsConnectionIds($points);


        foreach ($tracks as $track){
            $trackpttk = new TrackPTTK($track -> Id, $track -> Nazwa_trasy,
                $track -> PunktyWGore, $track -> PunktyWDol, $track -> ObowiązujeOd, $track -> ObowiązujeDo);

            $pointIndexes = [];

            for($j=0; $j < count($pointObjectConnectionIds);$j++){
                if ($pointObjectConnectionIds[$j] == $track -> Id){
                    array_push($pointIndexes, $j);
                }
            }

            $trackpttk -> setPointA($pointObjects[$pointIndexes[0]]);
            $trackpttk -> setPointB($pointObjects[$pointIndexes[1]]);


            array_push($trackObjects, $trackpttk);
        }

        return $trackObjects;
    }
    public static function pcreateTrackObjects($name,$tripPoints, $pointsUp, $pointsDown){
        $trackObject = new TrackPTTK(0, $name, $pointsUp, $pointsDown, date('Y-m-j'), null);


        $trackObject -> setPointA($tripPoints[0]);
        $trackObject -> setPointB($tripPoints[1]);

        return $trackObject;
    }
}