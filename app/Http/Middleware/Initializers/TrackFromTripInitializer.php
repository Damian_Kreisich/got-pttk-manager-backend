<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 05.01.2019
 * Time: 16:25
 */

namespace App\Http\Middleware\Initializers;


use App\Models\TrackFromTrip;

class TrackFromTripInitializer
{
    public static function createTFTObject($tracksFromTrip, $trackObjects){
        $tFTObjects = [];

        foreach($tracksFromTrip as $tft){
           $tftObject = new TrackFromTrip($tft -> Id, $tft -> DataOd, $tft -> DataDo, $tft -> CzyWlasna, $tft -> CzyWDol);

           $trackIndex = 0;
           for($i = 0; $i < count($trackObjects); $i++){
               if ($trackObjects[$i] -> getId() == $tft -> Trasa_PTTKId){
                   $trackIndex = $i;
                   break;
               }
           }
           $tftObject -> setTrack($trackObjects[$trackIndex]);


           array_push($tFTObjects, $tftObject);
        }
        return $tFTObjects;
    }
    public static function createTFTConnectionIds($tracksFromTrip){
        $TFTConIds = [];

        foreach ($tracksFromTrip as $tft) {
            array_push($TFTConIds, $tft -> WycieczkaId);
        }
        return $TFTConIds;
    }
}