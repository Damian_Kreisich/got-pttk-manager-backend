<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 06.01.2019
 * Time: 00:10
 */

namespace App\Http\Middleware\Initializers;

use App\Models\MountainGroup;

class MountainGroupInitializer
{
    public static function createMountainGroupObjects($mountainGroups){
        $mountainGroupObjects = [];
        foreach($mountainGroups as $mg){
            $mountainGroupObject = new MountainGroup($mg -> Id, $mg -> GrupaGórska);

            array_push($mountainGroupObjects, $mountainGroupObject);
        }
        return $mountainGroupObjects;
    }
}