<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 05.01.2019
 * Time: 23:30
 */

namespace App\Http\Middleware\Initializers;


use App\Models\Badge;

class BadgeInitializer
{
    public static function createBadgeObject($badges){
        $badgeObjects = [];
        foreach($badges as $badge) {
            $badgeObject = new Badge($badge->Id, $badge->DataZdobycia, $badge->DataRozpoczecia, $badge->PotrzebnePunkty,
                $badge->normy, $badge->Stopien, $badge->Rodzaj_odznaki);
            array_push($badgeObjects,$badgeObject);
        }
        return $badgeObjects;
    }
}