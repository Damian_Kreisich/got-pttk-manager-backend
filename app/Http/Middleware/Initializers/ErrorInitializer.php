<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 06.01.2019
 * Time: 12:29
 */

namespace App\Http\Middleware\Initializers;


use App\Models\TrackError;

class ErrorInitializer
{
    public static function createErrorObjects($description, $priority, $photo, $cords, $touristId){
        if ($cords[0] != null and $cords[1] != null)
            $cords = CordsInitializer::createCordsObjectsFromStringData($cords[0][0], $cords[0][1], $cords [0][2],
                $cords[1][0], $cords[1][1], $cords[1][2]);

        $latitude = $cords[0];
        $longitude = $cords[1];

        $errorObject = new TrackError(0, date('Y-m-j'), $photo, $description, $priority, $latitude,
            $longitude, $touristId);


        return $errorObject;
    }
}