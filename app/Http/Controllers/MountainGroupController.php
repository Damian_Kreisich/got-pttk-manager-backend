<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 06.01.2019
 * Time: 02:14
 */

namespace App\Http\Controllers;


use App\Http\Middleware\DAO\MountainGroupDAO;
use App\Http\Middleware\Initializers\MountainGroupInitializer;
use App\Http\Middleware\Utility;

class MountainGroupController extends Controller{
    public  function SGetAllMountainGroups(){
        $mountainGroupObjects = MountainGroupInitializer::createMountainGroupObjects(MountainGroupDAO::getAllMountainGroups());

        $json = Utility::JSON_with_polish_chars($mountainGroupObjects);
        return response($json);
    }
}