<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 06.01.2019
 * Time: 00:55
 */

namespace App\Http\Controllers;

use App\Http\Middleware\DAO\CordsDAO;
use App\Http\Middleware\Initializers\CordsInitializer;
use App\Http\Middleware\DAO\PointDAO;
use App\Http\Middleware\Initializers\PointsInitializer;
use App\Http\Middleware\Utility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class PointsController extends Controller
{
    public function SGetPointByMountainGroupId($id){
        $points = PointDAO::getPointsByMountainGroupId($id);
        $pointsIds = [];
        foreach ($points as $point){
            array_push($pointsIds, $point -> Id);
        }

        $cords = CordsDAO::getCordsByPointIds($pointsIds);

        $cordsObjects = CordsInitializer::createCordsObject($cords);
        $cordsConIds = CordsInitializer::createCordsConnectionIds($cords);

        $pointObjects = PointsInitializer::createPointObject($points, $cordsObjects, $cordsConIds);

        $json = Utility::JSON_with_polish_chars($pointObjects);
        return response($json);
    }
    public function SGetPointsByTrackPointsValueAndMountainGroupId($pointsValue, $operator, $mountainGroupId){
        $operator = Utility::getOperatorSignFromString($operator);

        $points = PointDAO::getPointsByPointsValueAndMountainGroup($pointsValue, $operator, $mountainGroupId);
        $pointsIds = [];
        foreach ($points as $point){
            array_push($pointsIds, $point -> Id);
        }

        $cords = CordsDAO::getCordsByPointIds($pointsIds);

        $cordsObjects = CordsInitializer::createCordsObject($cords);
        $cordsConIds = CordsInitializer::createCordsConnectionIds($cords);

        $pointObjects = PointsInitializer::createPointObject($points, $cordsObjects, $cordsConIds);

        $json = Utility::JSON_with_polish_chars($pointObjects);
        return response($json);
    }
    public function PostPoint(Request $request){
        $name = $request -> get ("name");
        $mountainGroup = $request -> get("mountainGroup");
        $latitudedegrees = Input::get('latitudedegrees');
        $latitudeminutes = Input::get('latitudeminutes');
        $latitudeseconds = Input::get('latitudeseconds');
        $longitudedegrees = Input::get('longitudedegrees');
        $longitudeminutes = Input::get('longitudeminutes');
        $longitudeseconds = Input::get('longitudeseconds');
        $altitude = $request -> get("altitude");

        $latitude = [$latitudedegrees, $latitudeminutes, $latitudeseconds];
        $longitude = [$longitudedegrees, $longitudeminutes, $longitudeseconds];
        $cords = [$latitude, $longitude];

        print_r($cords);

        $PointObjects = PointsInitializer::pcreatePointObject(0, $name, $altitude, $cords, $mountainGroup, 1);

        return response(PointDAO::insertPointObject($PointObjects));
    }
}