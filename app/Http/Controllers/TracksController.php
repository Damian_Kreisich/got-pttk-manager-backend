<?php

namespace App\Http\Controllers;

use App\Http\Middleware\DAO\CordsDAO;
use App\Http\Middleware\DAO\PointDAO;
use App\Http\Middleware\Initializers\TracksInitializer;
use App\Http\Middleware\DAO\TrackDAO;
use App\Http\Middleware\Utility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class TracksController extends Controller
{


    public function STrackByName($strName){
        $strName = Utility::replace_WithSpace($strName);

        $tracks = TrackDAO::getTrackByPointOrTrackName($strName);

        $trackIds = array();
        foreach($tracks as $track){
            array_push($trackIds, $track->Id);
        }

        $points = PointDAO::getPointsByTrackIds($trackIds);

        $pointIds = array();

        foreach($points as $point){
            array_push($pointIds, $point->Id);
        }

        $cords = CordsDAO::getCordsByPointIds($pointIds);

        $objects = TracksInitializer::createTrackObject($tracks, $points, $cords);


        $json = Utility::JSON_with_polish_chars($objects);
        return response($json);
    }

    public function STrackById($id){
        $tracks = TrackDAO::getTrackById($id);

        $trackIds = [$id];

        $points = PointDAO::getPointsByTrackIds($trackIds);

        $pointIds = array();

        foreach($points as $point){
            array_push($pointIds, $point->Id);
        }

        $cords = CordsDAO::getCordsByPointIds($pointIds);

        $objects = TracksInitializer::createTrackObject($tracks, $points, $cords);

        $json = Utility::JSON_with_polish_chars($objects);
        return response($json);
    }

    public function SGetTrackByPointId($id){
        $tracks = TrackDAO::getTrackByPointId($id);
        $trackids = [];

        foreach($tracks as $track){
            array_push($trackids, $track -> Id);
        }

        $points = PointDAO::getPointsByTrackIds($trackids);
        $pointids = [];
        foreach($points as $point){
            array_push($pointids, $point -> Id);
        }

        $cords = CordsDAO::getCordsByPointIds($pointids);

        $objects = TracksInitializer::createTrackObject($tracks, $points, $cords);
        $finalObjects = null;

        $json = Utility::JSON_with_polish_chars($objects);
        return response($json);
    }

    public function PostTrack(Request $request){
        $name = Input::get('name');
        $pointA = Input::get('pointA');
        $pointB = Input::get('pointB');
        $pointsUp = Input::get('pointsUp');
        $pointsDown = Input::get('pointsDown');

        $points = [$pointA, $pointB];

        $trackObject = TracksInitializer::pcreateTrackObjects($name, $points,$pointsUp, $pointsDown);

        return response(TrackDAO::insertTrackObject($trackObject));
        #return Utility::JSON_with_polish_chars($trackObject);
    }
    public function updateTrack(Request $request){
        $id = Input::get("id");

        return response(TrackDaO::updateTrackExpDateById($id));
    }
}

