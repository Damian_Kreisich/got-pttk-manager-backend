<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 06.01.2019
 * Time: 12:27
 */

namespace App\Http\Controllers;


use App\Http\Middleware\DAO\ErrorDAO;
use App\Http\Middleware\Initializers\ErrorInitializer;
use App\Http\Middleware\Utility;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ErrorsController extends Controller
{
    public function PutError($description,$priority,$photo,$cords, $touristId){
        $ErrorObjects = ErrorInitializer::createErrorObjects($description, $priority, $photo, $cords, $touristId);

        $json = Utility::JSON_with_polish_chars($ErrorObjects);
        return response($json, 200);
    }
    public function PostError(Request $request){
        $description = Input::get('description');
        $priority = Input::get('priority');
        $photo = $request -> file('evidence');
        $latitudedegrees = Input::get('latitudedegrees');
        $latitudeminutes = Input::get('latitudeminutes');
        $latitudeseconds = Input::get('latitudeseconds');
        $longitudedegrees = Input::get('longitudedegrees');
        $longitudeminutes = Input::get('longitudeminutes');
        $longitudeseconds = Input::get('longitudeseconds');
        $touristId = Input::get('declarant');
        $evidence = null;
        if ($photo != null)
         $evidence = Utility::savePhotoOnDisc($photo);
        $latitude = [$latitudedegrees, $latitudeminutes, $latitudeseconds];
        $longitude = [$longitudedegrees, $longitudeminutes, $longitudeseconds];
        $cords = [$latitude, $longitude];
        $ErrorObjects = ErrorInitializer::createErrorObjects($description, $priority, $evidence, $cords, $touristId);

        return response(ErrorDAO::insertErrorObject($ErrorObjects));
    }
}