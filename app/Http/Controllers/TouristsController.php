<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 05.01.2019
 * Time: 15:45
 */

namespace App\Http\Controllers;


use App\Http\Middleware\DAO\CordsDAO;
use App\Http\Middleware\DAO\PointDAO;
use App\Http\Middleware\DAO\TouristDAO;
use App\Http\Middleware\Initializers\TouristInitializer;
use App\Http\Middleware\DAO\TrackDAO;
use App\Http\Middleware\DAO\TrackFromTripDAO;
use App\Http\Middleware\DAO\TripDAO;
use App\Http\Middleware\Utility;

class TouristsController extends Controller
{
    public function STouristByName($name){
        #$name = Utility::replace_WithSpace($name);

        $tourists = TouristDAO::getTouristByName($name);

        $touristsIds = [];

        foreach($tourists as $tourist){
            array_push($touristsIds, $tourist->Id);
        }

        $trips = TripDAO::getTripsByTouristsId($touristsIds);
        $tripsIds = [];
        foreach($trips as $trip){
            array_push($tripsIds, $trip->Id);
        }
        $tracksFromTrip = TrackFromTripDAO::getTrackFromTripByTripId($tripsIds);
        $trackIds = [];
        foreach($tracksFromTrip as $tfT){
            array_push( $trackIds, $tfT -> Trasa_PTTKId);
        }
        $tracks = TrackDAO::getTrackById($trackIds);

        $points = PointDAO::getPointsByTrackIds($trackIds);

        $pointIds = [];
        foreach($points as $point){
            array_push($pointIds, $point->Id);
        }

        $cords = CordsDAO::getCordsByPointIds($pointIds);

        $objects = TouristInitializer::createTouristObject($tourists, $trips, $tracksFromTrip, $tracks, $points, $cords);

        $json = Utility::JSON_with_polish_chars($objects);
        return response($json);
    }
    public function STouristById($id){
        $tourists = TouristDAO::getTouristById($id);

        $touristsIds = [];

        foreach($tourists as $tourist){
            array_push($touristsIds, $tourist->Id);
        }

        $trips = TripDAO::getTripsByTouristsId($touristsIds);
        $tripsIds = [];
        foreach($trips as $trip){
            array_push($tripsIds, $trip->Id);
        }
        $tracksFromTrip = TrackFromTripDAO::getTrackFromTripByTripId($tripsIds);
        $trackIds = [];
        foreach($tracksFromTrip as $tfT){
            array_push( $trackIds, $tfT -> Trasa_PTTKId);
        }
        $tracks = TrackDAO::getTrackById($trackIds);

        $points = PointDAO::getPointsByTrackIds($trackIds);

        $pointIds = [];
        foreach($points as $point){
            array_push($pointIds, $point->Id);
        }

        $cords = CordsDAO::getCordsByPointIds($pointIds);

        $objects = TouristInitializer::createTouristObject($tourists, $trips, $tracksFromTrip, $tracks, $points, $cords);

        $json = Utility::JSON_with_polish_chars($objects);
        return response($json);
    }
}