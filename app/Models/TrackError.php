<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 06.01.2019
 * Time: 12:34
 */

namespace App\Models;


class TrackError implements \jsonSerializable
{
    private $id;
    private $filingDate;
    private $evidence;
    private $description;
    private $priority;
    private $latitude;
    private $longitude;
    private $declarant;//touristObjectId

    /**
     * TrackError constructor.
     * @param $id
     * @param $filingDate
     * @param $evidence
     * @param $description
     * @param $priority
     * @param $latitude
     * @param $longitude
     */
    public function __construct($id, $filingDate, $evidence, $description, $priority, $latitude, $longitude, $declarant)
    {
        $this->id = $id;
        $this->filingDate = $filingDate;
        $this->evidence = $evidence;
        $this->description = $description;
        $this->priority = $priority;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->declarant = $declarant;
    }

    /**
     * @return mixed
     */
    public function getDeclarant()
    {
        return $this->declarant;
    }

    /**
     * @param mixed $declarant
     */
    public function setDeclarant($declarant): void
    {
        $this->declarant = $declarant;
    }


    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getFilingDate()
    {
        return $this->filingDate;
    }

    /**
     * @param mixed $filingDate
     */
    public function setFilingDate($filingDate): void
    {
        $this->filingDate = $filingDate;
    }

    /**
     * @return mixed
     */
    public function getEvidence()
    {
        return $this->evidence;
    }

    /**
     * @param mixed $evidence
     */
    public function setEvidence($evidence): void
    {
        $this->evidence = $evidence;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param mixed $priority
     */
    public function setPriority($priority): void
    {
        $this->priority = $priority;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude): void
    {
        $this->longitude = $longitude;
    }

}