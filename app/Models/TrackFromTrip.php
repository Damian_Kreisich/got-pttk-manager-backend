<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 05.01.2019
 * Time: 16:16
 */

namespace App\Models;


class TrackFromTrip implements \JsonSerializable
{
    private $id;
    private $startDate;
    private $endDate;
    private $isCustom;
    private $isUp;
    private $track;

    /**
     * TrackFromTrip constructor.
     * @param $id
     * @param $startDate
     * @param $endDate
     * @param $isCustom
     * @param $isUp
     */
    public function __construct($id, $startDate, $endDate, $isCustom, $isUp)
    {
        $this->id = $id;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->isCustom = $isCustom;
        $this->isUp = $isUp;
    }

    /**
     * @return mixed
     */
    public function getTrack()
    {
        return $this->track;
    }

    /**
     * @param mixed $track
     */
    public function setTrack($track): void
    {
        $this->track = $track;
    }


    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}