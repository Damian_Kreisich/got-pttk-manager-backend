<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 05.01.2019
 * Time: 15:37
 */

namespace App\Models;


class Tourist implements \JsonSerializable
{
    private $id;
    private $birthDate;
    private $points;
    private $identityCardNumber;
    private $isDisabled;
    private $isForeman;
    private $name;
    private $surname;
    private $PESEL;
    private $phoneNumber;
    private $residenceCity;
    private $residenceProvince;
    private $latestBadge;
    private $trips; #trip list
    private $priviliges;

    /**
     * @return mixed
     */
    public function getPriviliges()
    {
        return $this->priviliges;
    }

    /**
     * @param mixed $priviliges
     */
    public function setPriviliges($priviliges): void
    {
        $this->priviliges = $priviliges;
    }

    /**
     * @return mixed
     */
    public function getLatestBadge()
    {
        return $this->latestBadge;
    }

    /**
     * @param mixed $latestBadge
     */
    public function setLatestBadge($latestBadge): void
    {
        $this->latestBadge = $latestBadge;
    }

    /**
     * @return mixed
     */
    public function getTrips()
    {
        return $this->trips;
    }

    /**
     * @param mixed $trips
     */
    public function setTrips($trips): void
    {
        $this->trips = $trips;
    }

    /**
     * Tourist constructor.
     * @param $id
     * @param $birthDate
     * @param $points
     * @param $identityCardNumber
     * @param $isDisabled
     * @param $isForeman
     * @param $name
     * @param $surname
     * @param $PESEL
     * @param $phoneNumber
     * @param $residenceCity
     * @param $residenceProvince
     */
    public function __construct($id, $birthDate, $points, $identityCardNumber, $isDisabled, $isForeman, $name, $surname,
                                $PESEL, $phoneNumber, $residenceCity, $residenceProvince)
    {
        $this->id = $id;
        $this->birthDate = $birthDate;
        $this->points = $points;
        $this->identityCardNumber = $identityCardNumber;
        $this->isDisabled = $isDisabled;
        $this->isForeman = $isForeman;
        $this->name = $name;
        $this->surname = $surname;
        $this->PESEL = $PESEL;
        $this->phoneNumber = $phoneNumber;
        $this->residenceCity = $residenceCity;
        $this->residenceProvince = $residenceProvince;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }


}