<?php

namespace App\Models;


abstract class Track
{
    protected $id;
    protected $name;
    protected $pointA;
    protected $pointB;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPointA()
    {
        return $this->pointA;
    }

    /**
     * @param mixed $pointA
     */
    public function setPointA($pointA): void
    {
        $this->pointA = $pointA;
    }

    /**
     * @return mixed
     */
    public function getPointB()
    {
        return $this->pointB;
    }

    /**
     * @param mixed $pointB
     */
    public function setPointB($pointB): void
    {
        $this->pointB = $pointB;
    }
    public function __construct($id, $name){
        $this -> id = $id;
        $this -> name = $name;
    }
}
