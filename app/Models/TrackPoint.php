<?php

namespace App\Models;


class TrackPoint implements \jsonSerializable
{
    private $id;
    private $name;
    private $altitude;
    private $latitude;
    private $longitude;
    private $mountainGroup;
    private $country;
    /**
     * @return mixed
     */
    public function __construct($id, $name, $altitude, $mountainGroup, $country){
        $this -> id = $id;
        $this -> name = $name;
        $this -> altitude = $altitude;
        $this -> mountainGroup = $mountainGroup;
        $this -> country = $country;
    }
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param mixed $country
     */
    public function setCountry($country): void
    {
        $this->country = $country;
    }

    /**
     * @return mixed
     */
    public function getAltitude()
    {
        return $this->altitude;
    }

    /**
     * @param mixed $altitude
     */
    public function setAltitude($altitude): void
    {
        $this->altitude = $altitude;
    }

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude): void
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude): void
    {
        $this->longitude = $longitude;
    }

    /**
     * @return mixed
     */
    public function getMountainGroup()
    {
        return $this->mountainGroup;
    }

    /**
     * @param mixed $mountainGroup
     */
    public function setMountainGroup($mountainGroup): void
    {
        $this->mountainGroup = $mountainGroup;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
