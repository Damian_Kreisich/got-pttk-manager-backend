<?php

namespace App\Models;


class PointCoordinates implements \JsonSerializable
{
    private $id;
    private $degrees;
    private $minutes;
    private $seconds;

    public function __construct($id, $degrees, $minutes, $seconds){
        $this -> id = $id;
        $this -> degrees = $degrees;
        $this -> minutes = $minutes;
        $this -> seconds = $seconds;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getDegrees()
    {
        return $this->degrees;
    }

    /**
     * @param mixed $degrees
     */
    public function setDegrees($degrees): void
    {
        $this->degrees = $degrees;
    }

    /**
     * @return mixed
     */
    public function getMinutes()
    {
        return $this->minutes;
    }

    /**
     * @param mixed $minutes
     */
    public function setMinutes($minutes): void
    {
        $this->minutes = $minutes;
    }

    /**
     * @return mixed
     */
    public function getSeconds()
    {
        return $this->seconds;
    }

    /**
     * @param mixed $seconds
     */
    public function setSeconds($seconds): void
    {
        $this->seconds = $seconds;
    }
    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
