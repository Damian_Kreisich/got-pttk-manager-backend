<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 05.01.2019
 * Time: 23:16
 */

namespace App\Models;


class Badge implements \JsonSerializable
{
    private $id;
    private $acquisitionDate;
    private $startDate;
    private $pointsRequired;
    private $specialNorms;
    private $level;
    private $type;

    /**
     * Badge constructor.
     * @param $acquisitionDate
     * @param $startDate
     * @param $pointsRequired
     * @param $specialNorms
     * @param $level
     * @param $type
     */
    public function __construct($id, $acquisitionDate, $startDate, $pointsRequired, $specialNorms, $level, $type)
    {
        $this->id = $id;
        $this->acquisitionDate = $acquisitionDate;
        $this->startDate = $startDate;
        $this->pointsRequired = $pointsRequired;
        $this->specialNorms = $specialNorms;
        $this->level = $level;
        $this->type = $type;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}