<?php

namespace App\Models;





class TrackPTTK extends Track implements \JsonSerializable
{
    private $pointsUp;
    private $pointsDown;
    private $validFrom;
    private $validTo;
    public function __construct($id, $name,$pointsUp, $pointsDown, $validFrom, $validTo){
        Track::__construct($id, $name);
        $this -> pointsUp = $pointsUp;
        $this -> pointsDown = $pointsDown;
        $this -> validFrom = $validFrom;
        $this -> validTo = $validTo;
    }
    public function jsonSerialize()
    {
        $array = get_object_vars($this);

        return array_merge(['@type' => "TrackPTTK"], $array);
    }

    /**
     * @return mixed
     */
    public function getPointsUp()
    {
        return $this->pointsUp;
    }

    /**
     * @param mixed $pointsUp
     */
    public function setPointsUp($pointsUp): void
    {
        $this->pointsUp = $pointsUp;
    }

    /**
     * @return mixed
     */
    public function getPointsDown()
    {
        return $this->pointsDown;
    }

    /**
     * @param mixed $pointsDown
     */
    public function setPointsDown($pointsDown): void
    {
        $this->pointsDown = $pointsDown;
    }

    /**
     * @return mixed
     */
    public function getValidFrom()
    {
        return $this->validFrom;
    }

    /**
     * @param mixed $validFrom
     */
    public function setValidFrom($validFrom): void
    {
        $this->validFrom = $validFrom;
    }

    /**
     * @return mixed
     */
    public function getValidTo()
    {
        return $this->validTo;
    }

    /**
     * @param mixed $validTo
     */
    public function setValidTo($validTo): void
    {
        $this->validTo = $validTo;
    }

}
