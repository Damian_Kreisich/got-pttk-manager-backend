<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 06.01.2019
 * Time: 00:09
 */

namespace App\Models;


class MountainGroup implements \JsonSerializable
{
    private $id;
    private $name;

    /**
     * MountainGroup constructor.
     * @param $id
     * @param $name
     */
    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}