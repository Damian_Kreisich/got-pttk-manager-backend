<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 05.01.2019
 * Time: 15:49
 */

namespace App\Models;


class Trip implements \JsonSerializable
{
    private $id;
    private $startDate;
    private $endDate;
    private $name;
    private $points;
    private $tracks;#List of tracksFromTrip

    /**
     * Trip constructor.
     * @param $id
     * @param $startDate
     * @param $endDate
     * @param $name
     * @param $points
     */
    public function __construct($id, $startDate, $endDate, $name, $points)
    {
        $this->id = $id;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->name = $name;
        $this->points = $points;
    }

    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * @return mixed
     */
    public function getTracks()
    {
        return $this->tracks;
    }

    /**
     * @param mixed $tracks
     */
    public function setTracks($tracks): void
    {
        $this->tracks = $tracks;
    }

}