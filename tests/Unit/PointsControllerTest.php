<?php
/**
 * Created by PhpStorm.
 * User: Master
 * Date: 20.01.2019
 * Time: 11:42
 */

namespace Tests\Unit;

use App\Http\Controllers\PointsController;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class PointsControllerTest extends TestCase
{

    public function testPostPointBadCall()
    {
        $response = $this -> post('/api/point', [
            'name' => 'Kaszuby',
            'mountainGroup' => 1,
            'altitude' => 1500,
            'latitude' => '{  
                            "id":0,
                            "degrees":49,
                            "minutes":10,
                            "seconds":46.5
                            }',
            'longitude' => '{  
                             "id":0,
                             "degrees":20,
                             "minutes":5,
                             "seconds":16.9   
      }'
        ]);
        $response -> assertSeeText(false);
    }
    public function testPostPointGoodCallWithUniqueData()
    {
        $response = $this -> post('/api/point', [
            'name' => 'Kaszuby',
            'mountainGroup' => 3,
            'latitudedegrees' => 49,
            'latitudeminutes' => 10,
            'latitudeseconds' => 46.5,
            'longitudedegrees' => 20,
            'longitudeminutes' => 5,
            'longitudeseconds' => 16.0,
            'altitude' => 1500
        ]);
        $response ->assertStatus(200);//pointAdded
        $response ->assertSeeText('true');
        DB::rollBack();
    }
    public function testPostPointGoodCallWithExistingName(){
        $response = $this -> post('/api/point', [
            'name' => 'Rusinowa Polana',
            'mountainGroup' => 1,
            'latitudedegrees' => 49,
            'latitudeminutes' => 10,
            'latitudeseconds' => 46.5,
            'longitudedegrees' => 20,
            'longitudeminutes' => 5,
            'longitudeseconds' => 16.0,
            'altitude' => 1500
        ]);
        $response -> assertSeeText('false');
    }

    public function testSGetPointByMountainGroupIdAskForUnregisteredMountainGroup()
    {
        $response = $this -> get('/api/points/id/8');
        $this -> assertEquals($response -> getContent(), '[]');
    }
    public function testSGetPointByMountainGroupIdGoodCall(){
        $response = $this -> get('/api/points/id/1');
        $response -> assertJsonCount(10);
    }
}
